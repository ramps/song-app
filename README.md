
### What is this repository for? ###

This is a showcase Android project presenting the MVVM architecture.
Application allows the user to search for songs by title, artist and
release year. Application searches for song in two independent data
sources: iTunes and local file with list of songs. Results are merged
into single list presented to the user.

### General Overview ###

The architecture of the project is depicted on the following diagram:

![Overview](song-catalog.png)
### Used Libraries ###

* Dagger2
* Lombok
* RxJava2
* Retrofit
* Android Databinding