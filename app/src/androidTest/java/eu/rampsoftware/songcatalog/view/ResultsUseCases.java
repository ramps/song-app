package eu.rampsoftware.songcatalog.view;

import android.support.test.espresso.ViewInteraction;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;

import eu.rampsoftware.songcatalog.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class ResultsUseCases {

    public static void navigateToSearchScreen() {
        ViewInteraction actionMenuItemView = onView(allOf(withId(R.id.action_search), withContentDescription("Search"), isDisplayed()));
        actionMenuItemView.perform(click());
    }

    public static void hasListItemWithText(String text) {
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.songs_list),
                        childAtPosition(
                                childAtPosition(
                                        IsInstanceOf.instanceOf(android.view.View.class),
                                        0),
                                2),
                        isDisplayed()));
        recyclerView.check(matches(isDisplayed()));

        recyclerView.check(matches(hasDescendant(withText(text))));
    }

    public static void hasResultDesciption(String description) {
        ViewInteraction resultDescription = onView(allOf(withId(R.id.songs_results_description), withText(description), isDisplayed()));
        resultDescription.check(matches(withText(description)));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
