package eu.rampsoftware.songcatalog.view;

import android.support.test.espresso.ViewInteraction;

import eu.rampsoftware.songcatalog.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.pressImeActionButton;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

public class SearchUseCases {

    public static void performLocalSearch(String keyword) {

        ViewInteraction localSourceCheck = onView(allOf(withId(R.id.search_source_local_check), isDisplayed()));
        localSourceCheck.check(matches(isDisplayed()));

        ViewInteraction remoteSourceCheck = onView(allOf(withId(R.id.search_source_remote_check), isDisplayed()));
        remoteSourceCheck.check(matches(isDisplayed()));

        ViewInteraction titleIncludedCheck = onView(allOf(withId(R.id.search_parameter_title_check), isDisplayed()));
        titleIncludedCheck.check(matches(isDisplayed()));

        ViewInteraction artistIncludedCheck = onView(allOf(withId(R.id.search_parameter_artist_check), isDisplayed()));
        artistIncludedCheck.check(matches(isDisplayed()));

        ViewInteraction releaseYearIncludedCheck = onView(allOf(withId(R.id.search_parameter_year_check), isDisplayed()));
        releaseYearIncludedCheck.check(matches(isDisplayed()));

        ViewInteraction appCompatCheckBox = onView(allOf(withId(R.id.search_source_remote_check), withText("Remote")));
        appCompatCheckBox.perform(scrollTo(), click());

        ViewInteraction appCompatEditText = onView(withId(R.id.search_query));
        appCompatEditText.perform(scrollTo(), click());

        ViewInteraction queryEditText = onView(withId(R.id.search_query));
        queryEditText.perform(scrollTo(), replaceText(keyword), closeSoftKeyboard());

        ViewInteraction textInputEditText3 = onView(allOf(withId(R.id.search_query)));
        textInputEditText3.perform(pressImeActionButton());
    }
}
