package eu.rampsoftware.songcatalog.view.main;


import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import eu.rampsoftware.songcatalog.view.ResultsUseCases;
import eu.rampsoftware.songcatalog.view.SearchUseCases;

@RunWith(AndroidJUnit4.class)
public class MainActivityUiTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void localSearchRequest() {
        ResultsUseCases.navigateToSearchScreen();

        SearchUseCases.performLocalSearch("Kryptonite");

        ResultsUseCases.hasListItemWithText("3 Doors Down");

        ResultsUseCases.hasResultDesciption("Resuts for keyword \"Kryptonite\":");
    }
}
