package eu.rampsoftware.songcatalog;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import eu.rampsoftware.songcatalog.di.ApplicationComponent;
import eu.rampsoftware.songcatalog.di.ApplicationModule;
import eu.rampsoftware.songcatalog.di.DaggerApplicationComponent;

public class SongCatalogApplication extends Application {

    private ApplicationComponent applicationComponent;

    @NonNull
    public static SongCatalogApplication get(@NonNull Context context) {
        return (SongCatalogApplication) context.getApplicationContext();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

}
