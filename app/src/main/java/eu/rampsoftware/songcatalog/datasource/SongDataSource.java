package eu.rampsoftware.songcatalog.datasource;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import io.reactivex.Observable;

public interface SongDataSource {
    Observable<SongData> readSongs(String query);
}
