package eu.rampsoftware.songcatalog.datasource.local;

import android.content.Context;
import android.support.annotation.VisibleForTesting;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.local.model.LocalSong;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import io.reactivex.Observable;


public class LocalSongDataSource implements SongDataSource {
    private final Context context;
    private final String assetFileName;
    private LocalSong[] songs;
    private Gson gson;

    public LocalSongDataSource(Context context, String assetFileName, final Gson gson) {
        this.gson = gson;
        this.context = context;
        this.assetFileName = assetFileName;
        initializeModel();
    }

    private void initializeModel() {
        InputStreamReader isr = null;
        try {
            isr = new InputStreamReader(openAsStream(assetFileName));
            songs = gson.fromJson(isr, LocalSong[].class);
        } catch (IOException e) {
            Log.v(logTag(), "Error while parsing json file from assets", e);
        } finally {
            if (null != isr) {
                try {
                    isr.close();
                } catch (IOException e) {
                    //NOP
                }
            }
        }
    }

    @VisibleForTesting
    protected InputStream openAsStream(String filename) throws IOException {
        return context.getAssets().open(filename);
    }

    private String logTag() {
        return "LocalSongDataSource";
    }

    @Override
    public Observable<SongData> readSongs(final String query) {
        return Observable.fromArray(songs)
                .map(localSong -> SongData.create(localSong.getSongClean(),
                        localSong.getARTISTCLEAN(), localSong.getReleaseYear()));
    }

}
