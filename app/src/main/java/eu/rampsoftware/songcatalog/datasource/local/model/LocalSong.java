package eu.rampsoftware.songcatalog.datasource.local.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocalSong {

    @SerializedName("Song Clean")
    @Expose
    private String songClean;
    @SerializedName("ARTIST CLEAN")
    @Expose
    private String aRTISTCLEAN;
    @SerializedName("Release Year")
    @Expose
    private Integer releaseYear;
    @SerializedName("COMBINED")
    @Expose
    private String cOMBINED;
    @SerializedName("First?")
    @Expose
    private Integer first;
    @SerializedName("Year?")
    @Expose
    private Integer year;
    @SerializedName("PlayCount")
    @Expose
    private Integer playCount;
    @SerializedName("F*G")
    @Expose
    private Integer fG;

    public String getSongClean() {
        return songClean;
    }

    public void setSongClean(String songClean) {
        this.songClean = songClean;
    }

    public String getARTISTCLEAN() {
        return aRTISTCLEAN;
    }

    public void setARTISTCLEAN(String aRTISTCLEAN) {
        this.aRTISTCLEAN = aRTISTCLEAN;
    }

    public Integer getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(Integer releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getCOMBINED() {
        return cOMBINED;
    }

    public void setCOMBINED(String cOMBINED) {
        this.cOMBINED = cOMBINED;
    }

    public Integer getFirst() {
        return first;
    }

    public void setFirst(Integer first) {
        this.first = first;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getPlayCount() {
        return playCount;
    }

    public void setPlayCount(Integer playCount) {
        this.playCount = playCount;
    }

    public Integer getFG() {
        return fG;
    }

    public void setFG(Integer fG) {
        this.fG = fG;
    }

}