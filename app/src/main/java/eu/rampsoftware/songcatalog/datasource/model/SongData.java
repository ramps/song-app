package eu.rampsoftware.songcatalog.datasource.model;

import android.os.Parcelable;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class SongData implements Parcelable{
    public static SongData create(String title, String artist, Integer year) {
        return new AutoValue_SongData(title, artist, year);
    }

    @Nullable
    public abstract String title();

    @Nullable
    public abstract String artist();

    @Nullable
    public abstract Integer releaseYear();
}
