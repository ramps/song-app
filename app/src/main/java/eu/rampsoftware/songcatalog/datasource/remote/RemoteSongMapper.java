package eu.rampsoftware.songcatalog.datasource.remote;

import android.support.annotation.NonNull;

import java.text.SimpleDateFormat;
import java.util.Locale;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSong;

public class RemoteSongMapper {

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy", Locale.US);

    public static SongData toSongData(@NonNull RemoteSong remoteSong) {
        Integer date = null;
        if (remoteSong.getReleaseDate() != null) {
            date = Integer.parseInt(DATE_FORMATTER.format(remoteSong.getReleaseDate()));
        }
        return SongData.create(remoteSong.getTrackName(), remoteSong.getArtistName(), date);
    }

}
