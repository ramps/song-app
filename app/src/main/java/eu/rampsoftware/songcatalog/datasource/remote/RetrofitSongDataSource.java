package eu.rampsoftware.songcatalog.datasource.remote;

import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSongList;
import io.reactivex.Observable;


public class RetrofitSongDataSource implements SongDataSource {

    private SongDataApi songDataApi;

    public RetrofitSongDataSource(final SongDataApi songDataApi) {
        this.songDataApi = songDataApi;
    }

    @Override
    public Observable<SongData> readSongs(final String query) {
        return songDataApi.getSongs(query)
                .map(RemoteSongList::getResults)
                .flatMapIterable(remoteSongs -> remoteSongs)
                .flatMap(remoteSong -> Observable.just(RemoteSongMapper.toSongData(remoteSong)));
    }

}
