package eu.rampsoftware.songcatalog.datasource.remote;

import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSongList;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SongDataApi {

    @GET("search")
    Observable<RemoteSongList> getSongs(@Query("term") String query);
}
