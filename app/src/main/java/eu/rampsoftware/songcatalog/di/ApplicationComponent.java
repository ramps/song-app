package eu.rampsoftware.songcatalog.di;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApplicationModule.class, RemoteDataSourceModule.class})
public interface ApplicationComponent {
    RetainedActivitySubcomponent newRetainedActivityComponent(RetainedActivityModule module);
}
