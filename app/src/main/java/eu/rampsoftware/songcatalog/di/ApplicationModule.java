package eu.rampsoftware.songcatalog.di;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.rampsoftware.songcatalog.SongCatalogApplication;
import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.local.LocalSongDataSource;
import eu.rampsoftware.songcatalog.repository.CombinedSongRepository;
import eu.rampsoftware.songcatalog.repository.SongRepository;
import eu.rampsoftware.songcatalog.utils.IntegerTypeAdapter;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.SchedulersProvider;

@Module
public class ApplicationModule {
    private final SongCatalogApplication application;

    public ApplicationModule(final SongCatalogApplication application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context provideContext(){
        return application;
    }

    @Singleton
    @Provides
    StringProvider provideStringProvider(){
        return new StringProvider(application);
    }

    @Singleton
    @Provides
    Gson provideGson(){
        return new GsonBuilder()
                .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                .registerTypeAdapter(Integer.class, new IntegerTypeAdapter())
                .create();
    }

    @Provides
    @Singleton
    @Named("local")
    SongDataSource provideLocalSongDataSource(Gson gson){
        return new LocalSongDataSource(application, "local-songs.json", gson);
    }

    @Provides
    @Singleton
    SongRepository provideSongRepository(@Named("local")SongDataSource localSource,
                                         @Named("remote")SongDataSource remoteSource){
        return new CombinedSongRepository(localSource, remoteSource);
    }

    @Provides
    @Singleton
    SchedulersProvider provideSchedulersProvider(){
        return new SchedulersProvider();
    }

}
