package eu.rampsoftware.songcatalog.di;

import com.google.gson.Gson;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.rampsoftware.songcatalog.BuildConfig;
import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.remote.RetrofitSongDataSource;
import eu.rampsoftware.songcatalog.datasource.remote.SongDataApi;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class RemoteDataSourceModule {
    @Provides
    @Singleton
    OkHttpClient providesHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(BuildConfig.DEBUG
                        ? HttpLoggingInterceptor.Level.BODY
                        : HttpLoggingInterceptor.Level.NONE))
                .build();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient httpClient, Gson gson){
        return new Retrofit.Builder()
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(httpClient)
                .baseUrl(BuildConfig.BASE_URL)
                .build();
    }

    @Provides
    @Singleton
    SongDataApi provideSongDataApi(Retrofit retrofit) {
        return retrofit.create(SongDataApi.class);
    }


    @Provides
    @Singleton
    @Named("remote")
    SongDataSource provideRetrofitSongDataSource(final SongDataApi songDataApi){
        return new RetrofitSongDataSource(songDataApi);
    }

}