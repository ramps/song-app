package eu.rampsoftware.songcatalog.di;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import eu.rampsoftware.songcatalog.repository.SongRepository;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.SchedulersProvider;
import eu.rampsoftware.songcatalog.view.details.viewmodel.DetailsViewModel;
import eu.rampsoftware.songcatalog.view.main.viewmodel.MainViewModel;
import eu.rampsoftware.songcatalog.view.search.viewmodel.SearchViewModel;

@Module
public class RetainedActivityModule {
    public RetainedActivityModule() {

    }

    @RetainedActivityScope
    @Provides
    @Named("activity")
    public String provideActivityTestString() {
        return "test-activity";
    }

    @RetainedActivityScope
    @Provides
    public MainViewModel provideMainViewModel(final StringProvider stringProvider, final SongRepository songRepository, final SchedulersProvider schedulersProvider) {
        return new MainViewModel(stringProvider, songRepository, schedulersProvider);
    }

    @RetainedActivityScope
    @Provides
    public SearchViewModel provideSearchViewModel(final StringProvider stringProvider) {
        return new SearchViewModel(stringProvider);
    }

    @RetainedActivityScope
    @Provides
    public DetailsViewModel provideDetailsViewModel() {
        return new DetailsViewModel();
    }
}
