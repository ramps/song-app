package eu.rampsoftware.songcatalog.di;

import dagger.Subcomponent;
import eu.rampsoftware.songcatalog.view.details.DetailsActivity;
import eu.rampsoftware.songcatalog.view.main.MainActivity;
import eu.rampsoftware.songcatalog.view.search.SearchActivity;

@RetainedActivityScope
@Subcomponent(modules = RetainedActivityModule.class)
public interface RetainedActivitySubcomponent {
    void inject(MainActivity mainActivity);

    void inject(SearchActivity searchActivity);

    void inject(DetailsActivity detailsActivity);
}
