package eu.rampsoftware.songcatalog.repository;

import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.repository.predicates.RxPredicates;
import eu.rampsoftware.songcatalog.repository.predicates.SongDataItemPredicate;
import io.reactivex.Observable;

import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_LOCAL;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_REMOTE;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_INCLUDED;

public class CombinedSongRepository implements SongRepository {

    private SongDataSource localSource;
    private SongDataSource remoteSource;

    public CombinedSongRepository(final SongDataSource localSource, final SongDataSource remoteSource) {
        this.localSource = localSource;
        this.remoteSource = remoteSource;
    }

    @Override
    public Observable<SongData> readSongs(final SearchCriteria criteria) {
        Observable<SongData> targetObservable;
        if (criteria.source() == SOURCE_LOCAL) {
            targetObservable = localSource.readSongs(criteria.query());
        } else if (criteria.source() == SOURCE_REMOTE) {
            targetObservable = remoteSource.readSongs(criteria.query());
        } else {
            targetObservable = Observable.merge(remoteSource.readSongs(criteria.query()),
                    localSource.readSongs(criteria.query()));
        }
        return targetObservable
                .filter(RxPredicates.or(
                        new SongDataItemPredicate(criteria.includeTitle() == TITLE_INCLUDED, criteria.query(), SongData::title),
                        new SongDataItemPredicate(criteria.includeArtist() == ARTIST_INCLUDED, criteria.query(), SongData::artist),
                        new SongDataItemPredicate(criteria.includeReleaseYear() == RELEASE_YEAR_INCLUDED, criteria.query(),
                                data -> data.releaseYear() == null ? null : Integer.toString(data.releaseYear()))
                        )
                ).distinct();

    }


}
