package eu.rampsoftware.songcatalog.repository;


import android.os.Parcelable;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;

import com.google.auto.value.AutoValue;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@AutoValue
public abstract class SearchCriteria implements Parcelable {
    public static final int SOURCE_LOCAL = 1;
    public static final int SOURCE_REMOTE = 2;
    public static final int SOURCE_BOTH = 3;
    public static final int TITLE_INCLUDED = 1;
    public static final int TITLE_EXCLUDED = 2;
    public static final int ARTIST_INCLUDED = 3;
    public static final int ARTIST_EXCLUDED = 4;
    public static final int RELEASE_YEAR_INCLUDED = 5;
    public static final int RELEASE_YEAR_EXCLUDED = 6;

    public static SearchCriteria create(String query,
                                        @TitleSearch int title,
                                        @ArtistSearch int artist,
                                        @ReleaseYearSearch int year,
                                        @Source int source) {
        return new AutoValue_SearchCriteria(query, title, artist, year, source);
    }

    @Nullable
    public abstract String query();

    @TitleSearch
    public abstract int includeTitle();

    @ArtistSearch
    public abstract int includeArtist();

    @ReleaseYearSearch
    public abstract int includeReleaseYear();

    @Source
    public abstract int source();
    @Retention(SOURCE)
    @IntDef({SOURCE_LOCAL, SOURCE_REMOTE, SOURCE_BOTH})
    public @interface Source {
    }
    @Retention(SOURCE)
    @IntDef({TITLE_INCLUDED, TITLE_EXCLUDED})
    public @interface TitleSearch {
    }
    @Retention(SOURCE)
    @IntDef({ARTIST_INCLUDED, ARTIST_EXCLUDED})
    public @interface ArtistSearch {
    }
    @Retention(SOURCE)
    @IntDef({RELEASE_YEAR_INCLUDED, RELEASE_YEAR_EXCLUDED})
    public @interface ReleaseYearSearch {
    }


}
