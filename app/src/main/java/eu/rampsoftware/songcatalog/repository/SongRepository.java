package eu.rampsoftware.songcatalog.repository;


import eu.rampsoftware.songcatalog.datasource.model.SongData;
import io.reactivex.Observable;

public interface SongRepository {
    Observable<SongData> readSongs(SearchCriteria criteria);
}
