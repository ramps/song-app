package eu.rampsoftware.songcatalog.repository.predicates;

import android.support.annotation.NonNull;

import io.reactivex.functions.Predicate;


public class RxPredicates<T> {

    public static <T> Predicate<T> and(@NonNull Predicate<T>... predicates) {
        return data -> {
            for (Predicate<T> predicate : predicates) {
                if (!predicate.test(data)) {
                    return false;
                }
            }
            return true;
        };
    }

    public static <T> Predicate<T> or(@NonNull Predicate<T>... predicates) {
        return data -> {
            for (Predicate<T> predicate : predicates) {
                if (predicate.test(data)) {
                    return true;
                }
            }
            return false;
        };
    }
}
