package eu.rampsoftware.songcatalog.repository.predicates;

import java.util.Locale;

import eu.rampsoftware.songcatalog.StringUtils;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.utils.Function;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Predicate;


public class SongDataItemPredicate implements Predicate<SongData> {

    private final Function<SongData, String> textExtractor;
    private final boolean shouldInclude;
    private final String query;

    public SongDataItemPredicate(final boolean shouldInclude, final String query, Function<SongData, String> textExtractor) {
        this.shouldInclude = shouldInclude;
        this.query = query;
        this.textExtractor = textExtractor;
    }

    @Override
    public boolean test(@NonNull final SongData songData) throws Exception {
        String textToCompare = textExtractor.apply(songData);
        if (!shouldInclude) {
            return false;
        }
        if (StringUtils.isEmpty(textToCompare)) {
            return false;
        }
        if (StringUtils.isEmpty(query)) {
            return true;
        }
        return textToCompare.toLowerCase(Locale.getDefault()).contains(query.toLowerCase(Locale.getDefault()));
    }
}
