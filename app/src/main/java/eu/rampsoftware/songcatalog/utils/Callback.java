package eu.rampsoftware.songcatalog.utils;

public interface Callback<T> {
    void call(T t);
}
