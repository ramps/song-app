package eu.rampsoftware.songcatalog.utils;

public interface Function<T,R> {
    R apply(T t);
}
