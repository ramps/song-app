package eu.rampsoftware.songcatalog.utils;

import android.content.Context;

public class StringProvider {
    private Context context;

    public StringProvider(final Context context) {
        this.context = context;
    }

    public String getString(int resourceId) {
        return context.getString(resourceId);
    }

    public String getString(int resourceId, Object...args) {
        return context.getString(resourceId, args);
    }
}
