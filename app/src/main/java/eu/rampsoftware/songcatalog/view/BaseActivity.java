package eu.rampsoftware.songcatalog.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import eu.rampsoftware.songcatalog.SongCatalogApplication;
import eu.rampsoftware.songcatalog.di.ApplicationComponent;

public abstract class BaseActivity<T, VM extends BaseViewModel> extends AppCompatActivity {

    private static final String KEY_IS_RETAINED = "KEY_IS_RETAINED";

    private T component;
    private VM viewModel;

    @Override
    public void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        component = getComponent();
        injectDependencies(component);
        viewModel = getViewModel();
        viewModel.registerNavigator(new Navigator(this));
        configureHomeBackButton();
    }

    private void configureHomeBackButton() {
        if (shouldDisplayHomeAsUp()) {
            final ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.setDisplayHomeAsUpEnabled(true);
            }
        }
    }

    protected boolean shouldDisplayHomeAsUp() {
        return false;
    }

    protected abstract VM getViewModel();

    protected abstract void injectDependencies(final T component);

    @Override
    protected void onStart() {
        super.onStart();
        viewModel.onStart();
    }

    @Override
    protected void onStop() {
        viewModel.onStop();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        viewModel.unregisterNavigator();
        if (!isChangingConfigurations()) {
            viewModel.onDestroy();
        }
        super.onDestroy();
    }

    protected T getComponent() {
        //noinspection unchecked
        component = (T) getLastCustomNonConfigurationInstance();
        if (null == component) {
            component = newComponent(SongCatalogApplication.get(this).getApplicationComponent());
        }
        return component;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(KEY_IS_RETAINED, isChangingConfigurations());
    }

    protected boolean isRetained(final Bundle state) {
        return state != null && state.containsKey(KEY_IS_RETAINED) && state.getBoolean(KEY_IS_RETAINED);
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance() {
        return component;
    }

    protected abstract T newComponent(ApplicationComponent applicationComponent);
}
