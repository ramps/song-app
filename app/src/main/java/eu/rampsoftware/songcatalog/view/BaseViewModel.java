package eu.rampsoftware.songcatalog.view;

import android.databinding.BaseObservable;

public abstract class BaseViewModel extends BaseObservable {

    protected Navigator navigator;

    public void onStart(){}

    public void onStop(){}

    public abstract void onDestroy();

    public void registerNavigator(Navigator navigator){
        this.navigator = navigator;
    }

    public void unregisterNavigator(){
        navigator = null;
    }
}