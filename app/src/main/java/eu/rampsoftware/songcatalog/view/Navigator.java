package eu.rampsoftware.songcatalog.view;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.view.details.DetailsActivity;
import eu.rampsoftware.songcatalog.view.search.SearchActivity;

public class Navigator {
    public static final String EXTRA_SEARCH_CRITERIA = "EXTRA_SEARCH_CRITERIA";
    public static final String EXTRA_SONG_DATA = "EXTRA_SONG_DATA";
    public static final int REQUEST_CODE_SEARCH_SCREEN = 1;
    private Activity activity;
    private ProgressDialog progress;

    public Navigator(final Activity activity) {
        this.activity = activity;
    }

    public void navigateToSearchScreen(SearchCriteria criteria) {
        dismissProgressDialog();
        Intent intent = new Intent(activity, SearchActivity.class);
        intent.putExtra(EXTRA_SEARCH_CRITERIA, criteria);
        activity.startActivityForResult(intent, REQUEST_CODE_SEARCH_SCREEN);
    }

    public void navigateToSongDetailsScreen(SongData songData) {
        dismissProgressDialog();
        Intent intent = new Intent(activity, DetailsActivity.class);
        intent.putExtra(EXTRA_SONG_DATA, songData);
        activity.startActivity(intent);
    }

    public void navigateBackFromSearch(SearchCriteria criteria) {
        dismissProgressDialog();
        Intent intent = new Intent();
        intent.putExtra(EXTRA_SEARCH_CRITERIA, criteria);
        activity.setResult(Activity.RESULT_OK, intent);
        activity.finish();
    }

    public void showProgressDialog(){
        if(progress != null && progress.isShowing()){
            return;
        }
        progress = new ProgressDialog(activity);
        progress.show();
    }

    public void dismissProgressDialog(){
        if(progress == null || !progress.isShowing()){
            return;
        }
        progress.dismiss();
        progress = null;
    }
}
