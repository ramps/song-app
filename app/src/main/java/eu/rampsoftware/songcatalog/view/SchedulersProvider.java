package eu.rampsoftware.songcatalog.view;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class SchedulersProvider {
    public Scheduler io(){
        return Schedulers.io();
    }

    public Scheduler mainThread(){
        return AndroidSchedulers.mainThread();
    }
}
