package eu.rampsoftware.songcatalog.view.details;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.di.ApplicationComponent;
import eu.rampsoftware.songcatalog.di.RetainedActivityModule;
import eu.rampsoftware.songcatalog.di.RetainedActivitySubcomponent;
import eu.rampsoftware.songcatalog.view.BaseActivity;
import eu.rampsoftware.songcatalog.view.Navigator;
import eu.rampsoftware.songcatalog.view.details.viewmodel.DetailsViewModel;

public class DetailsActivity extends BaseActivity<RetainedActivitySubcomponent, DetailsViewModel> {

    @Inject
    DetailsViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_details);
        final SongData searchCriteria = extractSearchCriteria(getIntent());
        viewModel.populate(searchCriteria);
        binding.setVariable(BR.model, viewModel);
    }

    @Nullable
    private SongData extractSearchCriteria(final Intent intent) {
        if (intent == null || !intent.hasExtra(Navigator.EXTRA_SONG_DATA)) {
            return null;
        }
        return intent.getParcelableExtra(Navigator.EXTRA_SONG_DATA);
    }


    @Override
    protected RetainedActivitySubcomponent newComponent(final ApplicationComponent applicationComponent) {
        return applicationComponent.newRetainedActivityComponent(new RetainedActivityModule());
    }

    @Override
    protected void injectDependencies(final RetainedActivitySubcomponent component) {
        component.inject(this);
    }

    @Override
    protected DetailsViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected boolean shouldDisplayHomeAsUp() {
        return true;
    }
}
