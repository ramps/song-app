package eu.rampsoftware.songcatalog.view.details.viewmodel;

import android.databinding.Bindable;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.view.BaseViewModel;

public class DetailsViewModel extends BaseViewModel {

    /**
     * Observables
     */
    private String title;
    private String artist;
    private String releaseYear;

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getArtist() {
        return artist;
    }

    public void setArtist(final String artist) {
        this.artist = artist;
        notifyPropertyChanged(BR.artist);
    }

    @Bindable
    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(final String releaseYear) {
        this.releaseYear = releaseYear;
        notifyPropertyChanged(BR.releaseYear);
    }


    @Override
    public void onDestroy() {

    }

    public void populate(final SongData songData) {
        setTitle(songData.title());
        setArtist(songData.artist());
        setReleaseYear(songData.releaseYear() == null ? "" : Integer.toString(songData.releaseYear()));
    }
}
