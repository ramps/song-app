package eu.rampsoftware.songcatalog.view.main;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import javax.inject.Inject;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.di.ApplicationComponent;
import eu.rampsoftware.songcatalog.di.RetainedActivityModule;
import eu.rampsoftware.songcatalog.di.RetainedActivitySubcomponent;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.view.BaseActivity;
import eu.rampsoftware.songcatalog.view.Navigator;
import eu.rampsoftware.songcatalog.view.main.viewmodel.MainViewModel;

public class MainActivity extends BaseActivity<RetainedActivitySubcomponent, MainViewModel> {

    @Inject
    MainViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setVariable(BR.model, viewModel);
    }


    @Override
    protected RetainedActivitySubcomponent newComponent(final ApplicationComponent applicationComponent) {
        return applicationComponent.newRetainedActivityComponent(new RetainedActivityModule());
    }

    @Override
    protected void injectDependencies(final RetainedActivitySubcomponent component) {
        component.inject(this);
    }

    @Override
    protected MainViewModel getViewModel() {
        return viewModel;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_search:
                viewModel.searchClicked();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        if(requestCode == Navigator.REQUEST_CODE_SEARCH_SCREEN){
            if(resultCode == RESULT_OK){
                SearchCriteria searchCriteria = data.getParcelableExtra(Navigator.EXTRA_SEARCH_CRITERIA);
                viewModel.performSearch(searchCriteria);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
