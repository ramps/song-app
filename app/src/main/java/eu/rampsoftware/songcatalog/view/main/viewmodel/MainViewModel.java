package eu.rampsoftware.songcatalog.view.main.viewmodel;

import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableList;
import android.support.annotation.NonNull;
import android.support.annotation.VisibleForTesting;

import java.util.Collections;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.StringUtils;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.repository.SongRepository;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.BaseViewModel;
import eu.rampsoftware.songcatalog.view.SchedulersProvider;
import eu.rampsoftware.songcatalog.view.main.viewmodel.comparator.StringSongPropertyComparator;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_BOTH;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.create;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_ARTIST_ASC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_ARTIST_DESC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_RELEASE_YEAR_ASC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_RELEASE_YEAR_DESC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_TITLE_ASC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.ORDER_TITLE_DESC;
import static eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations.SortOrder;

public class MainViewModel extends BaseViewModel {

    private final SchedulersProvider schedulersProvider;
    private final StringProvider stringProvider;
    private SongRepository songRepository;

    private CompositeDisposable disposables = new CompositeDisposable();
    private int sortOrder;
    /**
     * Observables
     */
    private ObservableList<SongItemViewModel> songs = new ObservableArrayList<>();
    private String searchResultLabel;
    private boolean progresVisible;
    private SearchCriteria searchCriteria;


    public MainViewModel(final StringProvider stringProvider,
                         final SongRepository songRepository,
                         final SchedulersProvider schedulersProvider) {
        this.songRepository = songRepository;
        this.schedulersProvider = schedulersProvider;
        this.stringProvider = stringProvider;
        initializeDefaults();
        notifyVisibilityChanged();
    }

    private void initializeDefaults() {
        this.searchCriteria = getDefaultSearchCriteria();
        this.sortOrder = ORDER_TITLE_ASC;
    }

    @NonNull
    private SearchCriteria getDefaultSearchCriteria() {
        return create("", TITLE_INCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH);
    }

    @Bindable
    public boolean isProgresVisible() {
        return progresVisible;
    }

    public void setProgresVisible(final boolean progresVisible) {
        this.progresVisible = progresVisible;
        if(progresVisible){
            navigator.showProgressDialog();
        } else {
            navigator.dismissProgressDialog();
        }
        notifyPropertyChanged(BR.progresVisible);
    }

    @Bindable
    public ObservableList<SongItemViewModel> getSongs() {
        return songs;
    }

    @Bindable
    public String getSearchResultLabel() {
        return searchResultLabel;
    }

    private void setSearchResultLabel(final String searchResultLabel) {
        this.searchResultLabel = searchResultLabel;
        notifyPropertyChanged(BR.searchResultLabel);
    }

    @Bindable
    public boolean isItemsVisible() {
        return !StringUtils.isEmpty(this.searchCriteria.query());
    }

    @Bindable
    public boolean isEmptyViewVisible() {
        return songs.size() == 0 && StringUtils.isEmpty(this.searchCriteria.query());
    }

    @Bindable
    public boolean isSortingVisible() {
        return songs.size() > 0 && !StringUtils.isEmpty(this.searchCriteria.query());
    }

    public void sortClicked(@SortOrder int sortOrder) {
        if(songs.size() == 0) {
            return;
        }

        setProgresVisible(true);
        this.sortOrder = sortOrder;
        switch (sortOrder) {
            case ORDER_TITLE_ASC:
                Collections.sort(songs, new StringSongPropertyComparator(true, SongItemViewModel::getTitle));
                break;
            case ORDER_TITLE_DESC:
                Collections.sort(songs, new StringSongPropertyComparator(false, SongItemViewModel::getTitle));
                break;
            case ORDER_ARTIST_ASC:
                Collections.sort(songs, new StringSongPropertyComparator(true, SongItemViewModel::getArtist));
                break;
            case ORDER_ARTIST_DESC:
                Collections.sort(songs, new StringSongPropertyComparator(false, SongItemViewModel::getArtist));
                break;
            case ORDER_RELEASE_YEAR_ASC:
                Collections.sort(songs, new StringSongPropertyComparator(true, SongItemViewModel::getReleaseYear));
                break;
            case ORDER_RELEASE_YEAR_DESC:
                Collections.sort(songs, new StringSongPropertyComparator(false, SongItemViewModel::getReleaseYear));
                break;
        }
        setProgresVisible(false);
    }

    public void searchClicked() {
        this.navigator.navigateToSearchScreen(searchCriteria);
    }

    public void performSearch(@NonNull SearchCriteria searchCriteria) {
        setProgresVisible(true);
        this.searchCriteria = searchCriteria;
        this.songs.clear();

        addDisposable(songRepository.readSongs(searchCriteria)
                .subscribeOn(schedulersProvider.io())
                .observeOn(schedulersProvider.mainThread())
                .subscribeWith(new DisposableObserver<SongData>() {
                    @Override
                    public void onNext(@io.reactivex.annotations.NonNull final SongData songData) {
                        addSong(songData);
                    }

                    @Override
                    public void onError(@io.reactivex.annotations.NonNull final Throwable e) {
                        searchError(e);
                    }

                    @Override
                    public void onComplete() {
                        searchCompleted();
                    }
                }));
    }

    private void addDisposable(final Disposable disposableObserver) {
        this.disposables.add(disposableObserver);
    }

    private void searchCompleted() {
        sortClicked(this.sortOrder);
        int resultDescriptionResId;
        if (songs.size() > 0) {
            resultDescriptionResId = R.string.result_description;
        } else {
            resultDescriptionResId = R.string.result_description_empty_set;
        }
        setSearchResultLabel(stringProvider.getString(resultDescriptionResId, this.searchCriteria.query()));
        setProgresVisible(false);
        notifyVisibilityChanged();
    }

    private void notifyVisibilityChanged() {
        notifyPropertyChanged(BR.emptyViewVisible);
        notifyPropertyChanged(BR.itemsVisible);
        notifyPropertyChanged(BR.sortingVisible);
    }

    private void searchError(final Throwable throwable) {
        setSearchResultLabel(stringProvider.getString(R.string.error_retrieving_data_failed));
        setProgresVisible(false);
        notifyVisibilityChanged();
    }

    private void addSong(SongData songData) {
        final SongItemViewModel songViewModel = SongItemMapper.map(songData);
        songViewModel.setItemClickedCallback(this::songItemClicked);
        this.songs.add(songViewModel);
    }

    public void songItemClicked(SongData songData) {
        navigator.navigateToSongDetailsScreen(songData);
    }

    @Override
    public void onDestroy() {
        this.disposables.dispose();
    }

    @Bindable
    public int getItemBindingId() {
        return BR.model;
    }

    @VisibleForTesting
    CompositeDisposable getDisposables() {
        return disposables;
    }
}
