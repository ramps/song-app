package eu.rampsoftware.songcatalog.view.main.viewmodel;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

public class ModelAnnotations {

    public static final int ORDER_TITLE_ASC = 1;
    public static final int ORDER_TITLE_DESC = 2;
    public static final int ORDER_ARTIST_ASC = 3;
    public static final int ORDER_ARTIST_DESC = 4;
    public static final int ORDER_RELEASE_YEAR_ASC = 5;
    public static final int ORDER_RELEASE_YEAR_DESC = 6;


    @Retention(SOURCE)
    @IntDef({ORDER_TITLE_ASC, ORDER_TITLE_DESC,
            ORDER_ARTIST_ASC, ORDER_ARTIST_DESC,
            ORDER_RELEASE_YEAR_ASC, ORDER_RELEASE_YEAR_DESC})
    @interface SortOrder {
    }

    @SortOrder
    public static int fromPosition(final int position){
        switch (position) {
            case 1:
                return ORDER_TITLE_DESC;
            case 2:
                return ORDER_ARTIST_ASC;
            case 3:
                return ORDER_ARTIST_DESC;
            case 4:
                return ORDER_RELEASE_YEAR_ASC;
            case 5:
                return ORDER_RELEASE_YEAR_DESC;
            default:
                return ORDER_TITLE_ASC;
        }

    }
}
