package eu.rampsoftware.songcatalog.view.main.viewmodel;

import eu.rampsoftware.songcatalog.datasource.model.SongData;

public class SongItemMapper {
    public static SongItemViewModel map(final SongData songData) {
        return new SongItemViewModel(songData);
    }
}
