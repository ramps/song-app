package eu.rampsoftware.songcatalog.view.main.viewmodel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.utils.Callback;

public class SongItemViewModel extends BaseObservable {
    private Callback<SongData> itemClickedCallback;
    private SongData songData;
    private String title;
    private String artist;
    private Integer releaseYear;

    private SongItemViewModel(final String title, final String artist, final Integer releaseYear) {
        setTitle(title);
        setArtist(artist);
        setReleaseYear(releaseYear);
    }

    public SongItemViewModel(final SongData songData) {
        this(songData.title(), songData.artist(), songData.releaseYear());
        this.songData = songData;
    }

    public void setItemClickedCallback( Callback<SongData> itemClickedCallback){
        this.itemClickedCallback = itemClickedCallback;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public String getArtist() {
        return artist;
    }

    public void setArtist(final String artist) {
        this.artist = artist;
        notifyPropertyChanged(BR.artist);
    }

    @Bindable
    public String getReleaseYear() {
        return releaseYear == null ? "" : Integer.toString(releaseYear);
    }

    public void setReleaseYear(final Integer releaseYear) {
        this.releaseYear = releaseYear;
        notifyPropertyChanged(BR.releaseYear);
    }

    public void itemClicked() {
        if(itemClickedCallback != null) {
            itemClickedCallback.call(this.songData);
        }
    }

}
