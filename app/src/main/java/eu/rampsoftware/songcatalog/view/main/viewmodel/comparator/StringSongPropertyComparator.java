package eu.rampsoftware.songcatalog.view.main.viewmodel.comparator;

import java.util.Comparator;

import eu.rampsoftware.songcatalog.StringUtils;
import eu.rampsoftware.songcatalog.utils.Function;
import eu.rampsoftware.songcatalog.view.main.viewmodel.SongItemViewModel;

public class StringSongPropertyComparator implements Comparator<SongItemViewModel> {
    private final Function<SongItemViewModel, String> dataExtractor;
    private boolean ascending;

    public StringSongPropertyComparator(final boolean ascending, Function<SongItemViewModel, String> dataExtractor) {
        this.ascending = ascending;
        this.dataExtractor = dataExtractor;

    }

    @Override
    public int compare(final SongItemViewModel item1, final SongItemViewModel item2) {
        String title1 = dataExtractor.apply(item1);
        String title2 = dataExtractor.apply(item2);
        int result;
        if (StringUtils.isEmpty(title1) && StringUtils.isEmpty(title2)) {
            result = 0;
        } else if (StringUtils.isEmpty(title1)) {
            result = -1;
        } else if (StringUtils.isEmpty(title2)) {
            result = 1;
        } else {
            result = title1.compareTo(title2);
        }
        return result * (ascending ? 1 : -1);
    }
}
