package eu.rampsoftware.songcatalog.view.search;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.di.ApplicationComponent;
import eu.rampsoftware.songcatalog.di.RetainedActivityModule;
import eu.rampsoftware.songcatalog.di.RetainedActivitySubcomponent;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.view.BaseActivity;
import eu.rampsoftware.songcatalog.view.Navigator;
import eu.rampsoftware.songcatalog.view.search.viewmodel.SearchViewModel;

public class SearchActivity extends BaseActivity<RetainedActivitySubcomponent, SearchViewModel> {

    @Inject
    SearchViewModel viewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_search);
        if(!isRetained(savedInstanceState)) {
            final SearchCriteria searchCriteria = extractSearchCriteria(getIntent());
            viewModel.populate(searchCriteria);
        }
        binding.setVariable(BR.model, viewModel);
    }

    @Nullable
    private SearchCriteria extractSearchCriteria(final Intent intent) {
        if(intent ==  null || !intent.hasExtra(Navigator.EXTRA_SEARCH_CRITERIA)){
            return null;
        }
        return intent.getParcelableExtra(Navigator.EXTRA_SEARCH_CRITERIA);
    }


    @Override
    protected RetainedActivitySubcomponent newComponent(final ApplicationComponent applicationComponent) {
        return applicationComponent.newRetainedActivityComponent(new RetainedActivityModule());
    }

    @Override
    protected void injectDependencies(final RetainedActivitySubcomponent component) {
        component.inject(this);
    }

    @Override
    protected SearchViewModel getViewModel() {
        return viewModel;
    }

    @Override
    protected boolean shouldDisplayHomeAsUp() {
        return true;
    }
}
