package eu.rampsoftware.songcatalog.view.search.viewmodel;

import android.databinding.Bindable;
import android.support.annotation.Nullable;

import eu.rampsoftware.songcatalog.BR;
import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.StringUtils;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.BaseViewModel;

import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_BOTH;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_LOCAL;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_REMOTE;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.create;

public class SearchViewModel extends BaseViewModel {

    private final StringProvider stringProvider;
    /**
     * Observables
     */
    private boolean sourceLocal;
    private boolean sourceRemote;
    private boolean title;
    private boolean artist;
    private boolean releaseYear;
    private String query;
    private String errorMessage;

    public SearchViewModel(StringProvider stringProvider) {
        this.stringProvider = stringProvider;
    }

    public void populate(@Nullable final SearchCriteria criteria){
        if(criteria == null){
            return;
        }
        setArtist(criteria.includeArtist() == ARTIST_INCLUDED);
        setTitle(criteria.includeTitle() == TITLE_INCLUDED);
        setReleaseYear(criteria.includeReleaseYear() == RELEASE_YEAR_INCLUDED);
        setSourceLocal(criteria.source() == SOURCE_LOCAL || criteria.source() == SOURCE_BOTH);
        setSourceRemote(criteria.source() == SOURCE_REMOTE || criteria.source() == SOURCE_BOTH);
        setQuery(criteria.query());
    }

    @Bindable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.errorMessage = errorMessage;
        notifyPropertyChanged(BR.errorMessage);
    }

    @Bindable
    public boolean isSourceLocal() {
        return sourceLocal;
    }

    public void setSourceLocal(final boolean sourceLocal) {
        this.sourceLocal = sourceLocal;
        notifyPropertyChanged(BR.sourceLocal);
    }

    @Bindable
    public boolean isSourceRemote() {
        return sourceRemote;
    }

    public void setSourceRemote(final boolean sourceRemote) {
        this.sourceRemote = sourceRemote;
        notifyPropertyChanged(BR.sourceRemote);
    }

    @Bindable
    public boolean isTitle() {
        return title;
    }

    public void setTitle(final boolean title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    @Bindable
    public boolean isArtist() {
        return artist;
    }

    public void setArtist(final boolean artist) {
        this.artist = artist;
        notifyPropertyChanged(BR.artist);
    }

    @Bindable
    public boolean isReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(final boolean releaseYear) {
        this.releaseYear = releaseYear;
        notifyPropertyChanged(BR.releaseYear);
    }

    @Bindable
    public String getQuery() {
        return query;
    }

    public void setQuery(final String query) {
        this.query = query;
        notifyPropertyChanged(BR.query);
    }

    public void performSearch() {
        if (!anySourceSelected()) {
            setErrorMessage(stringProvider.getString(R.string.error_no_source_selected));
            return;
        }
        if (!anyPropertySelected()) {
            setErrorMessage(stringProvider.getString(R.string.error_no_property_selected));
            return;
        }
        if (StringUtils.isEmpty(getQuery())) {
            setErrorMessage(stringProvider.getString(R.string.error_empty_query));
            return;
        }
        navigator.navigateBackFromSearch(toSearchCriteria(this));
    }

    private boolean anyPropertySelected() {
        return isArtist() || isTitle() || isReleaseYear();
    }

    private boolean anySourceSelected() {
        return isSourceLocal() || isSourceRemote();
    }

    private SearchCriteria toSearchCriteria(final SearchViewModel model) {
        int source = SOURCE_LOCAL;
        if (model.isSourceLocal() && model.isSourceRemote()) {
            source = SOURCE_BOTH;
        } else if (model.isSourceRemote()) {
            source = SOURCE_REMOTE;
        }
        return create(query,
                model.isTitle() ? TITLE_INCLUDED : TITLE_EXCLUDED,
                model.isArtist() ? ARTIST_INCLUDED : ARTIST_EXCLUDED,
                model.isReleaseYear() ? RELEASE_YEAR_INCLUDED : RELEASE_YEAR_EXCLUDED,
                source);
    }

    @Override
    public void onDestroy() {
    }
}
