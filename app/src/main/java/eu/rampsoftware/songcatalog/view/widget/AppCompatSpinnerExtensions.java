package eu.rampsoftware.songcatalog.view.widget;

import android.databinding.BindingAdapter;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.AdapterView;

import eu.rampsoftware.songcatalog.view.main.viewmodel.ModelAnnotations;

public class AppCompatSpinnerExtensions {

    public interface ItemSelectionListener {
        void itemSelected(int itemPosition);
    }

    @BindingAdapter(value = {"bind:itemSelected"})
    public static void setClickListener(AppCompatSpinner spinner, ItemSelectionListener listener){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> adapterView, final View view, final int position, final long id) {
                listener.itemSelected(ModelAnnotations.fromPosition(position));
            }

            @Override
            public void onNothingSelected(final AdapterView<?> adapterView) {
            }
        });
    }
}
