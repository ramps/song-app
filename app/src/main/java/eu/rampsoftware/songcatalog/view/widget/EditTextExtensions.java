package eu.rampsoftware.songcatalog.view.widget;

import android.app.Activity;
import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputEditText;
import android.view.MotionEvent;
import android.widget.EditText;

import eu.rampsoftware.songcatalog.view.utils.ViewUtils;

public class EditTextExtensions {

    private final static int DRAWABLE_LEFT = 0;
    private final static int DRAWABLE_TOP = 1;
    private final static int DRAWABLE_RIGHT = 2;
    private final static int DRAWABLE_BOTTOM = 3;

    @BindingAdapter(value = "bind:performImeAction")
    public static void registerImeAction(TextInputEditText editText, Runnable callback) {
        editText.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (editText.getContext() instanceof Activity) {
                ViewUtils.hideSoftKeyboard((Activity) editText.getContext());
            }
            callback.run();
            return true;
        });
    }

    @BindingAdapter(value = "bind:drawableEndClick")
    public static void registerTouchListener(TextInputEditText editText, Runnable callback) {
        editText.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (event.getX() >= (editText.getWidth() - editText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                    callback.run();
                    return true;
                }
            }
            return false;
        });
    }
}
