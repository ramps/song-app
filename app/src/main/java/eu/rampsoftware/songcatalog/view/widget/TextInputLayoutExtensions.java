package eu.rampsoftware.songcatalog.view.widget;

import android.databinding.BindingAdapter;
import android.support.design.widget.TextInputLayout;

public class TextInputLayoutExtensions {

    @BindingAdapter({"bind:errorMessage"})
    public static void setErrorMessage(TextInputLayout textInput, String errorMessage) {
        textInput.setError(errorMessage);
    }

}
