package eu.rampsoftware.songcatalog.view.widget;

import android.databinding.BindingAdapter;
import android.view.View;

public class ViewExtensions {

    @BindingAdapter({"bind:performClick"})
    public static void performClick(View view, Runnable clickListener){
        view.setOnClickListener(v -> clickListener.run());
    }

}
