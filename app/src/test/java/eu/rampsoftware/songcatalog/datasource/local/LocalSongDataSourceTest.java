package eu.rampsoftware.songcatalog.datasource.local;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import eu.rampsoftware.songcatalog.BuildConfig;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.utils.IntegerTypeAdapter;
import io.reactivex.Observable;

import static eu.rampsoftware.songcatalog.matchers.HasSongDataWithTitleMatcher.hasSongDataWithTitleMatcher;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 21, assetDir = "resources")
public class LocalSongDataSourceTest {

    private LocalSongDataSource localSource;

    @Before
    public void setUp() {
        initMocks(this);
        localSource = new TestableLocalSongDataSource(
                RuntimeEnvironment.application,
                "local-songs-fixture.json",
                new GsonBuilder()
                        .setDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                        .registerTypeAdapter(Integer.class, new IntegerTypeAdapter())
                        .create());
    }

    @Test
    public void thatDataLoadedOnInit() {
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> observable = localSource.readSongs(null);

        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(21));
        assertThat(results, hasSongDataWithTitleMatcher("Art For Arts Sake"));
    }

    @Test
    public void thatEmptyDateMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> observable = localSource.readSongs(null);

        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(21));
        final SongData songData = getSongDataByTitle(results, "Fantasy Girl");
        assertThat(songData, notNullValue());
        assertThat(songData.releaseYear(), nullValue());

    }

    private SongData getSongDataByTitle(@NonNull List<SongData> items, @NonNull String title) {
        for (SongData songData : items) {
            if (title.equalsIgnoreCase(songData.title())) {
                return songData;
            }
        }
        return null;
    }

    private static class TestableLocalSongDataSource extends LocalSongDataSource {

        public TestableLocalSongDataSource(final Context context, final String assetFileName, final Gson gson) {
            super(context, assetFileName, gson);
        }

        @Override
        protected InputStream openAsStream(final String filename) throws IOException {
            return this.getClass().getClassLoader().getResourceAsStream(filename);
        }
    }

}
