package eu.rampsoftware.songcatalog.datasource.remote;

import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSong;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNull;

public class RemoteSongMapperTest {
    private static final long MILLIS_22_01_2016__10_00 = 1453453200000L;

    @Before
    public void setUp(){
    }

    @Test
    public void thatNonNullDateMappedCorrectly(){
        RemoteSong remoteSong = new RemoteSong();
        remoteSong.setReleaseDate(new Date(MILLIS_22_01_2016__10_00));

        final SongData songData = RemoteSongMapper.toSongData(remoteSong);

        assertNotNull(songData);
        assertNotNull(songData.releaseYear());
        assertEquals(2016, songData.releaseYear().intValue());
    }

    @Test
    public void thatNullDateMappedCorrectly(){
        RemoteSong remoteSong = new RemoteSong();
        remoteSong.setReleaseDate(null);

        final SongData songData = RemoteSongMapper.toSongData(remoteSong);

        assertNotNull(songData);
        assertNull(songData.releaseYear());
    }
}
