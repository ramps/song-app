package eu.rampsoftware.songcatalog.datasource.remote;

import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSong;
import eu.rampsoftware.songcatalog.datasource.remote.model.RemoteSongList;
import io.reactivex.Observable;

import static eu.rampsoftware.songcatalog.matchers.HasSongDataWithTitleMatcher.hasSongDataWithTitleMatcher;
import static org.hamcrest.Matchers.equalToIgnoringCase;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class RetrofitSongDataSourceTest {
    private static final long MILLIS_22_01_2016__10_00 = 1453453200000L;
    RetrofitSongDataSource dataSource;

    @Mock
    SongDataApi apiMock;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        dataSource = new RetrofitSongDataSource(apiMock);
    }

    @Test
    public void thatNonNullDataMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        RemoteSongList remoteSongList = remoteSongList("Britney", "Toxic", new Date(MILLIS_22_01_2016__10_00));
        when(apiMock.getSongs(anyString())).thenReturn(Observable.fromArray(remoteSongList));

        final Observable<SongData> observable = dataSource.readSongs("Britney");
        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(1));
        assertThat(results, hasSongDataWithTitleMatcher("Toxic"));
        assertThat(results.get(0).releaseYear(), notNullValue());
    }

    @Test
    public void thatNullDateMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        RemoteSongList remoteSongList = remoteSongList("Jonny", "Free falling", null);
        when(apiMock.getSongs(anyString())).thenReturn(Observable.fromArray(remoteSongList));

        final Observable<SongData> observable = dataSource.readSongs("Britney");
        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(1));
        assertThat(results, hasSongDataWithTitleMatcher("Free falling"));
        assertThat(results.get(0).releaseYear(), nullValue());
    }

    @Test
    public void thatNullTitleMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        RemoteSongList remoteSongList = remoteSongList("Jonny", null, new Date(MILLIS_22_01_2016__10_00));
        when(apiMock.getSongs(anyString())).thenReturn(Observable.fromArray(remoteSongList));

        final Observable<SongData> observable = dataSource.readSongs("Britney");
        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(1));
        assertThat(results.get(0).artist(), equalToIgnoringCase("Jonny"));
    }

    @Test
    public void thatNullArtistMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        RemoteSongList remoteSongList = remoteSongList(null, "Title", new Date(MILLIS_22_01_2016__10_00));
        when(apiMock.getSongs(anyString())).thenReturn(Observable.fromArray(remoteSongList));

        final Observable<SongData> observable = dataSource.readSongs("Britney");
        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(1));
        assertThat(results.get(0).title(), equalToIgnoringCase("Title"));
    }

    @Test
    public void thatEmptyResultMappedCorrectly() {
        List<SongData> results = new ArrayList<>();

        RemoteSongList remoteSongList = emptySongList();
        when(apiMock.getSongs(anyString())).thenReturn(Observable.fromArray(remoteSongList));

        final Observable<SongData> observable = dataSource.readSongs("Britney");
        observable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(0));
    }

    @NonNull
    private RemoteSongList emptySongList() {
        RemoteSongList remoteSongList = new RemoteSongList();
        List<RemoteSong> remoteSongs = new ArrayList<>();
        remoteSongList.setResults(remoteSongs);
        return remoteSongList;
    }

    @NonNull
    private RemoteSongList remoteSongList(final String artist, final String title, final Date releaseDate) {
        RemoteSongList remoteSongList = new RemoteSongList();
        List<RemoteSong> remoteSongs = new ArrayList<>();
        RemoteSong remoteSong = new RemoteSong();
        remoteSong.setArtistName(artist);
        remoteSong.setTrackName(title);
        remoteSong.setReleaseDate(releaseDate);
        remoteSongs.add(remoteSong);
        remoteSongList.setResults(remoteSongs);
        return remoteSongList;
    }
}
