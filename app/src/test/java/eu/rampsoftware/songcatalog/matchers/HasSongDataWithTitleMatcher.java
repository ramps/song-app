package eu.rampsoftware.songcatalog.matchers;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.List;

import eu.rampsoftware.songcatalog.datasource.model.SongData;

public class HasSongDataWithTitleMatcher extends TypeSafeDiagnosingMatcher<List<SongData>> {


    private final String title;

    public HasSongDataWithTitleMatcher(final String title) {
        this.title = title;
    }

    public static HasSongDataWithTitleMatcher hasSongDataWithTitleMatcher(String title) {
        return new HasSongDataWithTitleMatcher(title);
    }

    @Override
    protected boolean matchesSafely(final List<SongData> items, final Description mismatchDescription) {
        for (SongData songData : items) {
            if(title.equalsIgnoreCase(songData.title())){
                return true;
            }
        }
        return false;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("Title does not match");
    }
}
