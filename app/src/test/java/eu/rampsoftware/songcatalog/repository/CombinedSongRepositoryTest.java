package eu.rampsoftware.songcatalog.repository;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import eu.rampsoftware.songcatalog.datasource.SongDataSource;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import io.reactivex.Observable;


import static eu.rampsoftware.songcatalog.matchers.HasSongDataWithTitleMatcher.hasSongDataWithTitleMatcher;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.*;
import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class CombinedSongRepositoryTest {

    CombinedSongRepository repository;

    @Mock
    SongDataSource localSource;

    @Mock
    SongDataSource remoteSource;

    @Before
    public void setUp(){
        initMocks(this);
        repository = new CombinedSongRepository(localSource, remoteSource);
        when(localSource.readSongs(anyString())).thenReturn(songObservable());
        when(remoteSource.readSongs(anyString())).thenReturn(songObservableRemote());
    }

    @Test
    public void thatSourceDataFilteredByTitle(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("Hej", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(4));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho Remote"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly Remote"));
    }

    @Test
    public void thatSourceDataContainsFullListWhenEmptyQuery(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(10));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho Remote"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly Remote"));
    }

    @Test
    public void thatDuplicateSourceDataItemsFiltered(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("Hej Sokoly", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(2));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly Remote"));
    }

    @Test
    public void thatSourceDataFilteredByArtist(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("Hej", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(2));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander"));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander Remote"));
    }


    @Test
    public void thatSourceDataFilteredByTitleAndArtist(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("Hej", TITLE_INCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(6));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly"));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Ho Remote"));
        assertThat(results, hasSongDataWithTitleMatcher("Hej Sokoly Remote"));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander Remote"));
    }

    @Test
    public void thatSourceDataFilteredByReleaseYear(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("2014", TITLE_EXCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_INCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        assertThat(results, notNullValue());
        assertThat(results, hasSize(4));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander"));
        assertThat(results, hasSongDataWithTitleMatcher("Anything"));
        assertThat(results, hasSongDataWithTitleMatcher("Salamander Remote"));
        assertThat(results, hasSongDataWithTitleMatcher("Anything Remote"));
    }

    @Test
    public void thatOnlyLocalDataSourceRequested(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository
                .readSongs(SearchCriteria.create("2014", TITLE_EXCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_INCLUDED, SOURCE_LOCAL));
        songObservable.subscribe(results::add);

        verify(localSource, times(1)).readSongs("2014");
        verifyZeroInteractions(remoteSource);
    }

    @Test
    public void thatOnlyRemoteDataSourceRequested(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("2014", TITLE_EXCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_INCLUDED, SOURCE_REMOTE));
        songObservable.subscribe(results::add);

        verify(remoteSource, times(1)).readSongs("2014");
        verifyZeroInteractions(localSource);
    }

    @Test
    public void thatBothDataSourcesRequested(){
        List<SongData> results = new ArrayList<>();

        final Observable<SongData> songObservable = repository.readSongs(SearchCriteria
                .create("2014", TITLE_EXCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_INCLUDED, SOURCE_BOTH));
        songObservable.subscribe(results::add);

        verify(remoteSource, times(1)).readSongs("2014");
        verify(localSource, times(1)).readSongs("2014");
    }

    private Observable<SongData> songObservable() {
        return Observable.fromArray(
                SongData.create("Hej Ho", "Stanta Claus", 2012),
                SongData.create("Hej Sokoly", "Slowianie", 2013),
                SongData.create("Salamander", "Dog eat dog hej", 2014),
                SongData.create("Nothing", "In common", 2015),
                SongData.create("Anything", "With common year", 2014),
                SongData.create("Hej Sokoly", "Slowianie", 2013)
        );
    }

    private Observable<SongData> songObservableRemote() {
        return Observable.fromArray(
                SongData.create("Hej Ho Remote", "Stanta Claus", 2012),
                SongData.create("Hej Sokoly Remote", "Slowianie", 2013),
                SongData.create("Salamander Remote", "Dog eat dog hej", 2014),
                SongData.create("Nothing Remote", "In common", 2015),
                SongData.create("Anything Remote", "With common year", 2014),
                SongData.create("Hej Sokoly Remote", "Slowianie", 2013)
        );
    }
}
