package eu.rampsoftware.songcatalog.repository.predicates;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import io.reactivex.functions.Predicate;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@SuppressWarnings("unchecked")
public class RxPredicatesTest {

    @Rule
    public ExpectedException thrown= ExpectedException.none();

    @Test
    public void thatTrueAndFalseFilteredByAND() throws Exception {
        assertFalse(RxPredicates.and(truePredicate(), falsePredicate()).test(notUsed()));
    }

    @Test
    public void thatTrueAndTrueNotFilteredByAND() throws Exception {
        assertTrue(RxPredicates.and(truePredicate(), truePredicate()).test(notUsed()));
    }

    @Test
    public void thatFalseAndFalseFilteredByAND() throws Exception {
        assertFalse(RxPredicates.and(falsePredicate(), falsePredicate()).test(notUsed()));
    }

    @Test
    public void thatExceptionThrownByANDIfNullPredicates() throws Exception {
        thrown.expect(Exception.class);
        assertFalse(RxPredicates.and(null).test(notUsed()));
    }



    @Test
    public void thatTrueAndFalseFilteredByOr() throws Exception {
        assertTrue(RxPredicates.or(truePredicate(), falsePredicate()).test(notUsed()));
    }


    @Test
    public void thatTrueAndTrueNotFilteredByOr() throws Exception {
        assertTrue(RxPredicates.or(truePredicate(), truePredicate()).test(notUsed()));
    }

    @Test
    public void thatFalseAndFalseFilteredByOr() throws Exception {
        assertFalse(RxPredicates.or(falsePredicate(), falsePredicate()).test(notUsed()));
    }

    @Test
    public void thatExceptionThrownByOrIfNullPredicates() throws Exception {
        thrown.expect(Exception.class);
        assertFalse(RxPredicates.or(null).test(notUsed()));
    }

    private boolean notUsed() {
        return true;
    }

    private Predicate<Boolean> truePredicate() {
        return aBoolean -> true;
    }

    private Predicate<Boolean> falsePredicate() {
        return aBoolean -> false;
    }
}
