package eu.rampsoftware.songcatalog.repository.predicates;

import org.junit.Before;
import org.junit.Test;

import eu.rampsoftware.songcatalog.datasource.model.SongData;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class SongDataItemPredicateTest {

    @Before
    public void setUp() {

    }

    @Test
    public void thatTitleNotFilteredWhenIncludedAndContains() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "title", data -> "Some title");
        assertTrue(predicate.test(songData()));
    }

    @Test
    public void thatTitleNotFilteredWhenIncludedAndContainsDifferentLetterSizes() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "TITLE", data -> "Some title");
        assertTrue(predicate.test(songData()));
    }

    @Test
    public void thatTitleFilteredWhenIncludedAndNotContains() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "not-exist", data -> "Some title");
        assertFalse(predicate.test(songData()));
    }

    @Test
    public void thatTitleFilteredWhenIncludedAndEmptyTitle() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "not-exist", data -> "");
        assertFalse(predicate.test(songData()));
    }

    @Test
    public void thatTitleFilteredWhenIncludedAndNullTitle() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "not-exist", data -> null);
        assertFalse(predicate.test(songData()));
    }

    @Test
    public void thatTitleNotFilteredWhenEmptyQuery() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, "", data -> "Some title");
        assertTrue(predicate.test(songData()));
    }

    @Test
    public void thatTitleNotFilteredWhenNullQuery() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(true, null, data -> "Some title");
        assertTrue(predicate.test(songData()));
    }

    @Test
    public void thatTitleFilteredWhenNotIncluded() throws Exception {
        final SongDataItemPredicate predicate = new SongDataItemPredicate(false, "not-exist", data -> "Some title");
        assertFalse(predicate.test(songData()));
    }


    private SongData songData(){
        return SongData.create("Title name", "artist name", 1999);
    }


}
