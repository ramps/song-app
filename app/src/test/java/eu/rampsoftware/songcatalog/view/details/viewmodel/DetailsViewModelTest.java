package eu.rampsoftware.songcatalog.view.details.viewmodel;

import org.junit.Before;
import org.junit.Test;

import eu.rampsoftware.songcatalog.datasource.model.SongData;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DetailsViewModelTest {
    private DetailsViewModel model;

    @Before
    public void setUp() {
        model = new DetailsViewModel();
    }

    @Test
    public void thatModelPopulatedWithNonEmptyData() {
        SongData songData = SongData.create("Title", "Artist", 1987);

        model.populate(songData);

        assertThat(model.getArtist(), is("Artist"));
        assertThat(model.getTitle(), is("Title"));
        assertThat(model.getReleaseYear(), is("1987"));
    }

    @Test
    public void thatModelPopulatedWithEmptyData() {
        SongData songData = SongData.create("", "", null);

        model.populate(songData);

        assertThat(model.getArtist(), is(""));
        assertThat(model.getTitle(), is(""));
        assertThat(model.getReleaseYear(), is(""));
    }

}
