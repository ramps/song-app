package eu.rampsoftware.songcatalog.view.main.viewmodel;

import android.databinding.ObservableList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.repository.SongRepository;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.Navigator;
import eu.rampsoftware.songcatalog.view.SchedulersProvider;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_BOTH;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_REMOTE;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.create;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class MainViewModelTest {

    @Mock
    SongRepository songRepository;
    @Mock
    SchedulersProvider schedulersProvider;
    @Mock
    StringProvider stringProvider;
    @Mock
    Navigator navigator;
    @Captor
    ArgumentCaptor<SearchCriteria> criteriaCaptor;
    @Captor
    private ArgumentCaptor<String> stringCaptor;
    @Captor
    private ArgumentCaptor<Integer> intCaptor;


    private MainViewModel model;

    @Before
    public void setUp() {
        initMocks(this);
        model = new MainViewModel(stringProvider, songRepository, schedulersProvider);
        model.registerNavigator(navigator);
    }

    @After
    public void tearDown() {
        model.unregisterNavigator();
    }

    @Test
    public void thatSongDataRequestPerformed() {
        when(songRepository.readSongs(any(SearchCriteria.class))).thenReturn(Observable.fromArray(nonEmptySongs()));
        when(schedulersProvider.mainThread()).thenReturn(Schedulers.trampoline());
        when(schedulersProvider.io()).thenReturn(Schedulers.trampoline());
        final SearchCriteria searchCriteria = create("test", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH);

        model.performSearch(searchCriteria);

        verify(songRepository, times(1)).readSongs(searchCriteria);
    }


    @Test
    public void thatSongListLoadedCorrectly() {
        when(songRepository.readSongs(any(SearchCriteria.class))).thenReturn(Observable.fromArray(nonEmptySongs()));
        when(schedulersProvider.mainThread()).thenReturn(Schedulers.trampoline());
        when(schedulersProvider.io()).thenReturn(Schedulers.trampoline());
        final SearchCriteria searchCriteria = create("test", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH);

        model.performSearch(searchCriteria);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
    }

    @Test
    public void thatPreviousSongListClearedBeforeNextRequest() {
        populateViewModel(model, nonEmptySongs());
        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));

        when(songRepository.readSongs(any(SearchCriteria.class))).thenReturn(Observable.fromArray(emptySongs()));
        when(schedulersProvider.mainThread()).thenReturn(Schedulers.trampoline());
        when(schedulersProvider.io()).thenReturn(Schedulers.trampoline());
        final SearchCriteria searchCriteria = create("test", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_BOTH);

        model.performSearch(searchCriteria);

        final ObservableList<SongItemViewModel> nextQuerySongs = model.getSongs();
        assertThat(nextQuerySongs, notNullValue());
        assertThat(nextQuerySongs.size(), is(equalTo(0)));
    }

    @Test
    public void thatSongListByDefaultSortedByTitleAscending() {
        populateViewModel(model);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Anything")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Salamander")));
    }

    @Test
    public void thatSongListSortedByTitleAscending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_TITLE_ASC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Anything")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Salamander")));
    }

    @Test
    public void thatSongListSortedByTitleDescending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_TITLE_DESC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Salamander")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Anything")));
    }

    @Test
    public void thatSongListSortedByArtistAscending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_ARTIST_ASC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Salamander")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Anything")));
    }

    @Test
    public void thatSongListSortedByArtistDescending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_ARTIST_DESC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Anything")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Salamander")));
    }

    @Test
    public void thatSongListSortedByReleaseDateAscending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_RELEASE_YEAR_ASC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Hej Ho")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Salamander")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Anything")));
    }

    @Test
    public void thatSongListSortedByReleaseDateDescending() {
        populateViewModel(model);

        model.sortClicked(ModelAnnotations.ORDER_RELEASE_YEAR_DESC);

        final ObservableList<SongItemViewModel> songs = model.getSongs();
        assertThat(songs, notNullValue());
        assertThat(songs.size(), is(equalTo(3)));
        assertThat(songs.get(0).getTitle(), is(equalTo("Anything")));
        assertThat(songs.get(1).getTitle(), is(equalTo("Salamander")));
        assertThat(songs.get(2).getTitle(), is(equalTo("Hej Ho")));
    }

    @Test
    public void thatDisposedWhenDestroyed() {
        populateViewModel(model);
        assertThat(model.getDisposables().isDisposed(), is(false));

        model.onDestroy();

        assertThat(model.getDisposables().isDisposed(), is(true));
    }

    @Test
    public void thatResultDescriptionsPopulatedCorrectly() {
        when(stringProvider.getString(anyInt(), any(Object.class))).thenReturn("Results for keyword \"Britney\":");

        populateViewModel(model);

        final String label = model.getSearchResultLabel();
        assertThat(label, is(equalTo("Results for keyword \"Britney\":")));
    }

    @Test
    public void thatCorrectViewsVisibleOnNonEmptyResultSet() {
        assertThat(model.isEmptyViewVisible(), is(true));
        assertThat(model.isItemsVisible(), is(false));
        assertThat(model.isSortingVisible(), is(false));

        populateViewModel(model);

        assertThat(model.isEmptyViewVisible(), is(false));
        assertThat(model.isItemsVisible(), is(true));
        assertThat(model.isSortingVisible(), is(true));
    }

    @Test
    public void thatCorrectViewsVisibleOnEmptyResultSet() {
        assertThat(model.isEmptyViewVisible(), is(true));
        assertThat(model.isItemsVisible(), is(false));
        assertThat(model.isSortingVisible(), is(false));

        populateViewModel(model, emptySongs());

        assertThat(model.isEmptyViewVisible(), is(false));
        assertThat(model.isItemsVisible(), is(true));
        assertThat(model.isSortingVisible(), is(false));
    }

    @Test
    public void thatNavigatesToSongDetailsOnSongItemClicked() {
        SongData song = SongData.create("Hej Ho", "Stanta Claus", 2012);
        populateViewModel(model, new SongData[]{song});
        assertThat(model.getSongs(), notNullValue());
        assertThat(model.getSongs().size(), is(equalTo(1)));

        model.getSongs().get(0).itemClicked();

        verify(navigator, times(1)).navigateToSongDetailsScreen(song);
    }

    @Test
    public void thatNavigatesToSearchScreenWithDefaultCriteriaWhenSerachClicked() {

        model.searchClicked();

        verify(navigator, times(1)).navigateToSearchScreen(criteriaCaptor.capture());
        final SearchCriteria criteria = criteriaCaptor.getValue();
        assertThat(criteria.query(), is(equalTo("")));
        assertThat(criteria.includeArtist(), is(equalTo(ARTIST_INCLUDED)));
        assertThat(criteria.includeReleaseYear(), is(equalTo(RELEASE_YEAR_EXCLUDED)));
        assertThat(criteria.includeTitle(), is(equalTo(TITLE_INCLUDED)));
        assertThat(criteria.source(), is(equalTo(SOURCE_BOTH)));
    }

    @Test
    public void thatNavigatesToSearchScreenWhenSerachClicked() {
        populateViewModel(model);

        model.searchClicked();

        verify(navigator, times(1)).navigateToSearchScreen(criteriaCaptor.capture());
        final SearchCriteria criteria = criteriaCaptor.getValue();
        assertThat(criteria.query(), is(equalTo("Britney")));
        assertThat(criteria.includeArtist(), is(equalTo(ARTIST_EXCLUDED)));
        assertThat(criteria.includeReleaseYear(), is(equalTo(RELEASE_YEAR_EXCLUDED)));
        assertThat(criteria.includeTitle(), is(equalTo(TITLE_INCLUDED)));
        assertThat(criteria.source(), is(equalTo(SOURCE_REMOTE)));
    }

    @Test
    public void thatProperMessageDisplayedOnEmptyResults() {
        when(stringProvider.getString(anyInt(), any())).thenReturn("No results found");
        populateViewModel(model, emptySongs());

        verify(stringProvider, times(1)).getString(intCaptor.capture(), stringCaptor.capture());
        assertThat(stringCaptor.getValue(), is("Britney"));
        assertThat(intCaptor.getValue(), is(R.string.result_description_empty_set));
        assertThat(model.getSearchResultLabel(), is(equalTo("No results found")));
    }

    @Test
    public void thatProperMessageDisplayedOnNonEmptyResults() {
        when(stringProvider.getString(anyInt(), any())).thenReturn("Results found");
        populateViewModel(model, nonEmptySongs());

        verify(stringProvider, times(1)).getString(intCaptor.capture(), stringCaptor.capture());
        assertThat(stringCaptor.getValue(), is("Britney"));
        assertThat(intCaptor.getValue(), is(R.string.result_description));
        assertThat(model.getSearchResultLabel(), is(equalTo("Results found")));
    }

    private void populateViewModel(final MainViewModel model) {
        populateViewModel(model, nonEmptySongs());
    }

    private void populateViewModel(final MainViewModel model, SongData[] songs) {
        when(songRepository.readSongs(any(SearchCriteria.class))).thenReturn(Observable.fromArray(songs));
        when(schedulersProvider.mainThread()).thenReturn(Schedulers.trampoline());
        when(schedulersProvider.io()).thenReturn(Schedulers.trampoline());
        final SearchCriteria searchCriteria = create("Britney", TITLE_INCLUDED, ARTIST_EXCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);
        model.performSearch(searchCriteria);
        final ObservableList<SongItemViewModel> songItems = this.model.getSongs();
        assertThat(songItems, notNullValue());
        assertThat(songItems.size(), is(equalTo(songs.length)));
    }

    private SongData[] nonEmptySongs() {
        return new SongData[]{
                SongData.create("Hej Ho", "Stanta Claus", 2012),
                SongData.create("Salamander", "Dog eat dog hej", 2014),
                SongData.create("Anything", "With common year", 2015)
        };
    }

    private SongData[] emptySongs() {
        return new SongData[]{};
    }
}
