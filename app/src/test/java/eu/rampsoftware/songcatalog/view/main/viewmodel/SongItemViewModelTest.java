package eu.rampsoftware.songcatalog.view.main.viewmodel;


import org.junit.Test;

import eu.rampsoftware.songcatalog.datasource.model.SongData;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class SongItemViewModelTest {
    SongItemViewModel model;


    @Test
    public void thatModelPopulatedCorrectly() {
        model = new SongItemViewModel(SongData.create("Title", "Artist", 1999));

        assertThat(model.getTitle(), is(equalTo("Title")));
        assertThat(model.getArtist(), is(equalTo("Artist")));
        assertThat(model.getReleaseYear(), is(equalTo("1999")));
    }
}
