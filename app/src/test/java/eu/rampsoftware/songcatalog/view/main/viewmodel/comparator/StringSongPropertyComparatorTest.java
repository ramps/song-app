package eu.rampsoftware.songcatalog.view.main.viewmodel.comparator;

import org.junit.Before;
import org.junit.Test;

import eu.rampsoftware.songcatalog.datasource.model.SongData;
import eu.rampsoftware.songcatalog.view.main.viewmodel.SongItemViewModel;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

public class StringSongPropertyComparatorTest {
    StringSongPropertyComparator comparator;

    @Before
    public void setUp() {
    }

    @Test
    public void thatLowerTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("aaaa", "", 0);
        SongItemViewModel item2 = item("zzzz", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(lessThan(0)));
    }

    @Test
    public void thatGreaterTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("zzzz", "", 0);
        SongItemViewModel item2 = item("aaaa", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(greaterThan(0)));
    }

    @Test
    public void thatEqualTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("bbb", "", 0);
        SongItemViewModel item2 = item("bbb", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(equalTo(0)));
    }


    @Test
    public void thatLowerTitleSortedCorrectlyDescending() {
        comparator = new StringSongPropertyComparator(false, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("aaaa", "", 0);
        SongItemViewModel item2 = item("zzzz", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(greaterThan(0)));
    }

    @Test
    public void thatGreaterTitleSortedCorrectlyDescending() {
        comparator = new StringSongPropertyComparator(false, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("zzzz", "", 0);
        SongItemViewModel item2 = item("aaaa", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(lessThan(0)));
    }

    @Test
    public void thatEqualTitleSortedCorrectlyDescending() {
        comparator = new StringSongPropertyComparator(false, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("bbb", "", 0);
        SongItemViewModel item2 = item("bbb", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(equalTo(0)));
    }


    @Test
    public void thatEmptyTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("", "", 0);
        SongItemViewModel item2 = item("bbb", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(lessThan(0)));
    }

    @Test
    public void thatEmptySecondTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("bbb", "", 0);
        SongItemViewModel item2 = item("", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(greaterThan(0)));
    }

    @Test
    public void thatNullTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item(null, "", 0);
        SongItemViewModel item2 = item("bbb", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(lessThan(0)));
    }

    @Test
    public void thatEmptySecondTitleSortedCorrectlyDescending() {
        comparator = new StringSongPropertyComparator(false, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("bbb", "", 0);
        SongItemViewModel item2 = item("", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(lessThan(0)));
    }

    @Test
    public void thatNullTitleSortedCorrectlyDescending() {
        comparator = new StringSongPropertyComparator(false, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item(null, "", 0);
        SongItemViewModel item2 = item("bbb", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(greaterThan(0)));
    }

    @Test
    public void thatNullSecondTitleSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("bbb", "", 0);
        SongItemViewModel item2 = item(null, "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(greaterThan(0)));
    }

    @Test
    public void thatEmptyTitlesSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item("", "", 0);
        SongItemViewModel item2 = item("", "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(equalTo(0)));
    }

    @Test
    public void thatNullTitlesSortedCorrectlyAscending() {
        comparator = new StringSongPropertyComparator(true, SongItemViewModel::getTitle);
        SongItemViewModel item1 = item(null, "", 0);
        SongItemViewModel item2 = item(null, "", 0);

        int result = comparator.compare(item1, item2);

        assertThat(result, is(equalTo(0)));
    }

    private SongItemViewModel item(final String title, final String artist, final Integer year) {
        return new SongItemViewModel(SongData.create(title, artist, year));
    }


}
