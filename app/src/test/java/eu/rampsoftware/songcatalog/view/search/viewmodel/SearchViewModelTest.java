package eu.rampsoftware.songcatalog.view.search.viewmodel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;

import eu.rampsoftware.songcatalog.R;
import eu.rampsoftware.songcatalog.StringUtils;
import eu.rampsoftware.songcatalog.repository.SearchCriteria;
import eu.rampsoftware.songcatalog.utils.StringProvider;
import eu.rampsoftware.songcatalog.view.Navigator;

import static eu.rampsoftware.songcatalog.repository.SearchCriteria.ARTIST_INCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.RELEASE_YEAR_EXCLUDED;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_LOCAL;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.SOURCE_REMOTE;
import static eu.rampsoftware.songcatalog.repository.SearchCriteria.TITLE_EXCLUDED;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

public class SearchViewModelTest {

    private SearchViewModel model;

    @Mock
    private StringProvider stringProvider;
    @Mock
    private Navigator navigator;
    @Captor
    private ArgumentCaptor<SearchCriteria> criteriaCaptor;

    @Before
    public void setUp() {
        initMocks(this);
        this.model = new SearchViewModel(stringProvider);
        this.model.registerNavigator(navigator);
    }


    @After
    public void tearDown() {
        this.model.unregisterNavigator();
    }

    @Test
    public void thatModelPopulatedCorrectly() {
        SearchCriteria criteria = SearchCriteria.create("query", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);

        model.populate(criteria);

        assertThat(model.isTitle(), is(false));
        assertThat(model.isArtist(), is(true));
        assertThat(model.isReleaseYear(), is(false));
        assertThat(model.isSourceRemote(), is(true));
        assertThat(model.isSourceLocal(), is(false));
        assertThat(model.getQuery(), is("query"));
    }

    @Test
    public void thatSearchNotAllowedIfNoPropertySelected() {
        when(stringProvider.getString(R.string.error_no_property_selected)).thenReturn("Error message");
        SearchCriteria criteria = SearchCriteria.create("query", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);
        model.populate(criteria);
        model.setTitle(false);
        model.setArtist(false);
        model.setReleaseYear(false);

        model.performSearch();

        verifyZeroInteractions(navigator);
        verify(stringProvider, times(1)).getString(R.string.error_no_property_selected);
        assertThat(model.getErrorMessage(), is(equalTo("Error message")));
    }

    @Test
    public void thatSearchNotAllowedIfNoSourceSelected() {
        when(stringProvider.getString(R.string.error_no_source_selected)).thenReturn("Error message");
        SearchCriteria criteria = SearchCriteria.create("query", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);
        model.populate(criteria);
        model.setSourceLocal(false);
        model.setSourceRemote(false);

        model.performSearch();

        verifyZeroInteractions(navigator);
        verify(stringProvider, times(1)).getString(R.string.error_no_source_selected);
        assertThat(model.getErrorMessage(), is(equalTo("Error message")));
    }

    @Test
    public void thatSearchNotAllowedIfEmptyQuery() {
        when(stringProvider.getString(R.string.error_empty_query)).thenReturn("Error message");
        SearchCriteria criteria = SearchCriteria.create("query", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);
        model.populate(criteria);
        model.setQuery("");

        model.performSearch();

        verifyZeroInteractions(navigator);
        verify(stringProvider, times(1)).getString(R.string.error_empty_query);
        assertThat(model.getErrorMessage(), is(equalTo("Error message")));
    }

    @Test
    public void thatNavigationBackPerformedWhenParametersCorrect() {
        when(stringProvider.getString(R.string.error_no_property_selected)).thenReturn("Error message");
        SearchCriteria criteria = SearchCriteria.create("query", TITLE_EXCLUDED, ARTIST_INCLUDED, RELEASE_YEAR_EXCLUDED, SOURCE_REMOTE);
        model.populate(criteria);
        model.setTitle(false);
        model.setArtist(true);
        model.setReleaseYear(false);
        model.setSourceLocal(true);
        model.setSourceRemote(false);
        model.setQuery("query");

        model.performSearch();

        assertThat(StringUtils.isEmpty(model.getErrorMessage()), is(true));
        verify(navigator, times(1)).navigateBackFromSearch(criteriaCaptor.capture());
        final SearchCriteria c = criteriaCaptor.getValue();
        assertThat(c.query(), is("query"));
        assertThat(c.includeArtist(), is(ARTIST_INCLUDED));
        assertThat(c.includeTitle(), is(TITLE_EXCLUDED));
        assertThat(c.includeReleaseYear(), is(RELEASE_YEAR_EXCLUDED));
        assertThat(c.source(), is(SOURCE_LOCAL));
    }

}
